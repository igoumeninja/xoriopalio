+++
title = "Σκάλπι"
date = "2017-09-25"
tags = [ "Προβατινολογικές-Ταυτότητες", "Πρόβατα", "Σκάλπι" ]
topics = [ "" ]
+++

<div class="org-center">
<a href="http://imgur.com/V8QjwA0"><img src="http://i.imgur.com/V8QjwA0.png" title="source: imgur.com" /></a>
</div>

O **Μυστηριώδης** *Σκάλπι*. Ευρέθει στο δρόμο χαμένος να περιφέρεται. Ο Μίτσο είπε αν μπορούσαμε να τον πιάσουμε γιατί θα τον φάνε τα σκυλιά. Έτσι έπειτα απο μία ώρα καταδίωξη μπλοκάρισμα τον πιάσαμε στο έθνικό στάδιο Γραικοχωρίου. Αυ6τό που είδα ήταν πολύ στενάχωρο. Μια πολύ βαθιά πληγή στο μέτωπο μ\\περι τα 4~5 εκατοστά γεμάτη σκουλήκια. Αυτό τον είχε ζουρλάνει. Τον πήραμε σπίτι έγινε η περίθαλψή (ακολουθεί λεπτομερώς παρακάτω) και σήμερα (<span class="timestamp-wrapper"><span class="timestamp">[2016-11-27 Sun]</span></span>) είναι πραγματικό ταυρί.

-   <span class="timestamp-wrapper"><span class="timestamp">[2017-08-25 Fri] </span></span> Έφκε για τον Μιχαλάκη
-   <span class="timestamp-wrapper"><span class="timestamp">[2017-05-01 Mon] </span></span> Έδρε επιβήτορας
-   <span class="timestamp-wrapper"><span class="timestamp">[2017-01-13 Fri] </span></span> Ο Σκάλπι γνωστός και ως παλιοΚώστας έφυγε για διαχειμανση στο Παραποτάμι, στο καταντιότου Michel Platini
-   <span class="timestamp-wrapper"><span class="timestamp">[2016-11-08 Tue] </span></span> 4ml Σελήνιο και εμβόλιο για την εντεροτοξιναιμία
-   <span class="timestamp-wrapper"><span class="timestamp">[2016-11-03 Thu] </span></span> Σπερέυ αντιβιωτικό
-   <span class="timestamp-wrapper"><span class="timestamp">[2016-10-23 Sun]</span></span>] Ευρεση του Σκάλπι, 3 μέρες αντιβίωση Micospectone απο 6 ml ενδομυικά, απομάκρυνση σκουληκιών με τσιμπιδάκι και μελάνι στο τραύμα για 1 βδομάδα.
